const readFile = (generateChart)=> {
    fetch('/output/matchesPerYear.json')
    .then((res) => res.json()).then((res) => {
        generateChart(res);
    });
}

readFile((data) => {
   const transformedMatchesPerYear = Object.values(data);  
   const transformedSeason = Object.keys(data);
   

   Highcharts.chart('totalMatches_container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Total Match'
    },
    subtitle: {
      text: 'Source: MountBlue.com'
    },
    xAxis: {
      categories: transformedSeason,
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Total matches played '
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: '',
      data: transformedMatchesPerYear
  
    }]
  });
});

const readMatchesWonPerSeasonFile = (generateChart)=> {
  fetch('/output/matchesWonPerSeasonPerTeam.json')
  .then((res) => res.json()).then((res) => {
      generateChart(res);
  });
}

readMatchesWonPerSeasonFile((data) => {
 const transformed = Object.values(data);
 const transformedSeason = Object.keys(data);


 Highcharts.chart('wonPerSeason_container', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Matches won per season per team'
  },
  subtitle: {
    text: 'Source: Mountblue.com'
  },
  xAxis: {
    categories: transformedSeason,
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Matches won per season per team'
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'Tokyo',
    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1]

  }, {
    name: 'New York',
    data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5]

  }, {
    name: 'London',
    data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2]

  }, {
    name: 'Berlin',
    data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1]

  }, {
    name: 'Tokyo',
    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1]

  }, {
    name: 'New York',
    data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5]

  }, {
    name: 'London',
    data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2]

  }, {
    name: 'Berlin',
    data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1]

  },{
    name: 'Tokyo',
    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1]

  }, {
    name: 'New York',
    data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5]

  }, {
    name: 'London',
    data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2]

  }, {
    name: 'Berlin',
    data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1]

  },{
    name: 'Tokyo',
    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1]

  }, {
    name: 'New York',
    data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5]

  }, {
    name: 'London',
    data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2]

  }]
});
});


const extraRunsConcReadFile = (generateChart)=> {
  fetch('/output/extraRuns.json')
  .then((res) => res.json()).then((res) => {
      generateChart(res);
  });
}

extraRunsConcReadFile((data) => {
 const transformedConcRuns = Object.values(data);
 const transformedTeam = Object.keys(data);
 

 Highcharts.chart('extraRunsConcContainer', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Extra Runs Conceded'
  },
  subtitle: {
    text: 'Source: MountBlue.com'
  },
  xAxis: {
    categories: transformedTeam,
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Extra Runs Conceded in 2016 '
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: '',
    data: transformedConcRuns

  }]
});
});


const data = [{name: "max payne", age: 23}, {name: "kay mane", age: 25}];
const output = ((array) => {
  return array.map(obj => {
    let tempArray = []
    let tempObj = {};
    const nameArray = obj.name.split(' ');
    tempArray.push(nameArray[0]);
    tempArray.push(nameArray[1]);
    tempObj.age = obj.age;
  })
});
const result = output(data);
console.log(result);

