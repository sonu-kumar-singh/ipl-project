const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('./src/data/matches.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    getMatchesPerSeason(results);
    getMatchesWonPerSeasonPerTeam(results);
    //console.log(results);
  });

  const getMatchesPerSeason = (results) => {
    const matchesPerSeason = {};
    for(let i=1;i<results.length;i++){
        if(matchesPerSeason.hasOwnProperty(results[i].season))
        matchesPerSeason[results[i].season]+=1;
        else
        matchesPerSeason[results[i].season]=1;
    }
   fs.writeFileSync('src/public/output/matchesPerYear.json', JSON.stringify(matchesPerSeason), {encoding: 'utf8'});
  }

  
  const getMatchesWonPerSeasonPerTeam = (results) => {
     const matchesWonPerSeasonPerTeam = {};
     const matchesWonPerTeam = {};
     results.forEach(matchInfoObj => {
      if(matchesWonPerSeasonPerTeam.hasOwnProperty(matchInfoObj.season)){
        matchesWonPerSeasonPerTeam[matchInfoObj.season][matchInfoObj.winner]+=1;
      }else{
        matchesWonPerTeam[matchInfoObj.winner] = 1;
        matchesWonPerSeasonPerTeam[matchInfoObj.season] = matchesWonPerTeam;
      }
      console.log(matchesWonPerSeasonPerTeam);
     });
     //console.log(matchesWonPerSeasonPerTeam);
    fs.writeFileSync('src/public/output/matchesWonPerSeasonPerTeam.json', JSON.stringify(matchesWonPerSeasonPerTeam), {encoding: 'utf8'});
  }
  
  