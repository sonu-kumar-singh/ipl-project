const fs = require('fs');
const path = require('path');

const deliveriesPath = path.join(__dirname, './src/data/deliveries.csv');
const matchesPath = path.join(__dirname, './src/data/matches.csv');


const transformIntoJSON = (data) =>{
   const lines = data.split('\n');
   const restData = lines.slice(1);
   const keys = lines[0].split(',');

   return restData.reduce((acc, eachLine) => {
   const eachLineData = eachLine.split(',');
   const obj = eachLineData.reduce((acc, eachData, index) => {
           acc[keys[index]] = eachData;
           return acc;
   }, {});
   acc.push(obj);
   return acc;
   }, []);
}

const readFileAndTransform = (filePath) => {
    const data = fs.readFileSync(filePath, {encoding: 'utf-8'});
    return transformIntoJSON(data);
}

const writeToFile = (filePath, data) => {
    fs.writeFileSync(filePath, JSON.stringify(data), {encoding: 'utf-8'});
}
const deliveriesData = readFileAndTransform(deliveriesPath);
writeToFile('src/data/deliveries.json', deliveriesData);
const matchesData = readFileAndTransform(matchesPath);
writeToFile('src/data/matches.json', matchesData);

//extra run conceded per team in 2016

const extraRunsConcededPerTeamIn2016 = (matchesData, deliveriesData) => {
    const allMatchesIn2016 = matchesData.reduce((acc, eachMatch) => {
        if(eachMatch.season === '2016'){
            acc[eachMatch.id]=true;
        }
        return acc;
    }, {});
    //console.log(allMatchesIn2016);

    return deliveriesData.reduce((acc, eachDelivery) => {
        if(allMatchesIn2016[eachDelivery.match_id]){
            if(acc[eachDelivery.bowling_team]){
                acc[eachDelivery.bowling_team]+=parseInt(eachDelivery.extra_runs, 10); 
            }else{
                acc[eachDelivery.bowling_team]=parseInt(eachDelivery.extra_runs, 10);
            }
        }
        return acc;
    }, {});
}
const data = extraRunsConcededPerTeamIn2016(matchesData, deliveriesData);
writeToFile(path.join(__dirname, 'src/public/output/extraRuns.json'), data);
